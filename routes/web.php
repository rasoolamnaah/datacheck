<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SchoolClassController;
use App\Http\Controllers\StudentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::view('class','schoolclass');
Route::get("class",[SchoolClassController::class,"addData"]);
Route::post("class",[SchoolClassController::class,"storeData"]);
//layout
Route::get("student",[StudentController::class,"addStudent"]);
//submit and save data
Route::post("save-student",[StudentController::class,"storeStudent"]);

Route::get("list-data",[StudentController::class,"selectData"]);
//delete route

Route::get("delete/{id}",[StudentController::class,"deleteStudent"]);

//edit route 
Route::get("edit/{id}",[StudentController::class,"showStudentData"]);

//submit post data
Route::post("edit-student",[StudentController::class,"submitEditStudent"]);
