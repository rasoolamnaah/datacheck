<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use Illuminate\Support\Facades\Redirect;

class StudentController extends Controller
{
    //
    public function addStudent()
    {
        return view("student");
    }
    public function selectData()
    {
       $students = Student::all();
       
        return view ("student",[
      "students"=>$students
      ]);
    }
    //delete method
    public function deleteStudent($student_id)
    {
           $student = Student::find($student_id);
           $student->delete();
           session()->flash("success","student record has been deleted successfully");
           return redirect("list-data");
}
//display student data
public function showStudentData($student_id)
{
    $student = Student::find($student_id);
    return view("show-data",[
        "student" => $student

    ]);
}
    public function submitEditStudent(Request $request)
    {
      //print_r($request->all());
      $student = Student::find($request['student_id']);
      //print_r($student);
      $student ->id = $request['id'];
      $student ->school_class_id = $request['school_class_id'];
      $student ->name= $request['name'];
      $student ->roll_no = $request['roll_no'];
      $student->save();

      session()->flash("success","student data has been updated successfully");
      return Redirect("list-data");
      
    }

    public function storeStudent(Request $request)
    {
        $student_obj = new Student;
        $student_obj->id= $request->id;
        $student_obj-> school_class_id= $request->school_class_id;
        $student_obj-> name= $request->name;
        $student_obj-> roll_no= $request->roll_no;
        $student_obj->save();

        //set flash message
        $request->session()->flash("success","Student record has been created");

        //redirection 
        return Redirect("student");
     
    }
}
