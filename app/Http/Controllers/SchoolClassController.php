<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SchoolClass;

class SchoolClassController extends Controller
{
    //
    public function addData(Request $req)
    {
        return view("schoolclass");
    }
    public function storeData(Request $req)
    {
        $SchoolClass= new SchoolClass;
        $SchoolClass->id=$req->id;
        $SchoolClass->class_name=$req->class_name;
        $SchoolClass->save();
        return redirect('schoolclass');

    }
    

}
