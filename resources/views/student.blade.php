@if(session()->has("success"))

<h3>{{ session("success")}}
</h3>
@endif
<h1>student data </h1>
<form method="POST" action="save-student">
    @csrf
    <p>
       ID : <input type="text" name="id">
    </p>
    <p>
        school_class_id : <input type="text" name="school_class_id">
    </p>
    <p>
        name : <input type="text" name="name">
    </p>
    <p>
        roll_no : <input type="text" name="roll_no">
    </p>
    <button type="submit">Submit </button>
</form>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>Student record</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link  rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>List students</h2>
  @if(session()->has("success"))
<div class="alert alert-success">
    {{session("success")}}
</div>
  @endif
  
  <table class="table" id="tbl-student">
    <thead>
      <tr>
        <th>id</th>
        <th>school_class_id</th>
        <th>name</th>
        <th>roll_no</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
       @foreach($students as $student)
       <tr>
        <td>{{ $student->id }}</td>
        <td>{{ $student->school_class_id }}</td>
        <td>{{ $student->name }}</td>
        <td>{{ $student->roll_no }}</td>
        <td>
            <a class="btn btn-info" href="edit/{{ $student->id }}">Edit</a>
            <a href="delete/{{ $student->id }}" class="btn btn-primary">Delete</a>
            
        </td>
      </tr>  
       @endforeach
     
     
      
    </tbody>
  </table>
</div>
<script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js">
</script>
<script>
$(document).ready( function () {
    $('#tbl-student').DataTable();
} );
    </script>
</body>
</html>
